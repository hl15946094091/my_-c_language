#define _CRT_SECURE_NO_WARNINGS 1
#pragma once

#include<assert.h>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>

#define NAME_MAX 20
#define SEX_MAX 5
#define ADDR_MAX 30
#define TELE_MAX 12
#define MAX 100
#define DEFAULT_SZ 3

typedef  struct PeoInfo
{
	char name[NAME_MAX];
	int age;
	char sex[SEX_MAX];
	char tele[TELE_MAX];
	char addr[ADDR_MAX];
}PeoInfo;

//静态初始化通讯录
//typedef struct Contact
//{
//	PeoInfo data[MAX];
//	int sz ;
//}Contact;


//动态版本
typedef struct Contact
{
	PeoInfo* data;
	int sz ;//记录通讯录当前人数
	int capacity;//记录通讯录当前容量


}Contact;

//初始化通讯录
void InitContact(Contact* pc);

//增加联系人
void AddContact(Contact* pc);

//显示联系人
void ShowContact(const Contact* pc);

//删除联系人
void DelContact(Contact* pc);

//查找指定联系人
void SearchContact(Contact* pc);

//修改联系人信息
void ModifyContact(Contact* pc);

//销毁通讯录
void DestroyContact(Contact* pc);

//保存通讯录
void SaveContact(Contact* pc);

//加载文件中数据到通讯录
void LoadContact(Contact* pc);
