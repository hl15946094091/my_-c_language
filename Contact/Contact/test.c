#define _CRT_SECURE_NO_WARNINGS 1
#include"Contact.h"

enum Option
{
	EXIT,
	ADD,
	DEL,
	SEARCH,
	MODIFY,
	SHOW,
	SORT
};


void menu()
{
	printf("********************************\n");
	printf("**** 1. add      2. del     ****\n");
	printf("**** 3. search   4. modify  ****\n");
	printf("**** 5. show     6. sort    ****\n");
	printf("**** 0. exit                ****\n");
	printf("********************************\n");
}

int main()
{
	int input = 0;
	Contact con; 
	//初始化通讯录
	InitContact(&con);
	do
	{
		menu();
		printf("< 请输入你的选择 >:");
		scanf("%d", &input);
		switch (input)
		{
		case EXIT:
			SaveContact(&con);
			DestroyContact(&con);
			printf("< 退出通讯录 >\n");
			break;
		case ADD:
			AddContact(&con);
			break;
		case DEL:
			DelContact(&con);
			break;
		case SEARCH:
			SearchContact(&con);
			break;
		case MODIFY:
			ModifyContact(&con);
			break;
		case SHOW:
			ShowContact(&con);
			break;
		case SORT:
			break;
		default:
			printf("< 选择错误重新选择！>\n");
			break;
		}
	} while (input);
	return 0;
}