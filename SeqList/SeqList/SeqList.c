#include"SeqList.h"

void SeqListInit(SL* psl)
{
	assert(psl);
	psl->array = NULL;
	psl->capacity = 0;
	psl->size = 0;
}

void SeqListPrint(SL* psl)
{
	assert(psl);
	int i = 0;
	for (i = 0; i < psl->size; i++)
	{
		printf("%d ", psl->array[i]);
	}
	printf("\n");
}

void SeqListDestory(SL* psl)
{
	assert(psl);
	if (psl->array != NULL)
	{
		free(psl->array);
		psl->array = NULL;
		psl->capacity = 0;
		psl->size = 0;
	}
}

// ���ռ䣬������ˣ���������
void CheckCapacity(SL* psl)
{
	assert(psl);
	if (psl->size == psl->capacity)
	{
		int newCapacity = (psl->capacity == 0) ? 4 : psl->capacity * 2;
		SeqListData* tmp = (SeqListData*)realloc(psl->array, sizeof(SeqListData) * newCapacity);
		if (tmp == NULL)
		{
			perror("realloc fail");
			return;
		}
		psl->array = tmp;
		psl->capacity = newCapacity;
	}
}

// ˳���β��
void SeqListPushBack(SL* psl, SeqListData x)
{
	assert(psl);
	CheckCapacity(psl);
	psl->array[psl->size] = x;
	psl->size++;
}

// ˳���ͷ��
void SeqListPushFront(SL* psl,SeqListData x)
{
	assert(psl);
	CheckCapacity(psl);
	int end = psl->size - 1;
	while (end >= 0)
	{
		psl->array[end + 1] = psl->array[end];
		end--;
	}
	psl->array[0] = x;
	psl->size++;
}

// ˳���βɾ
void SeqListPopBack(SL* psl)
{
	assert(psl);
	assert(psl->size>0);
	psl->size--;
}

// ˳���ͷɾ
void SeqListPopFront(SL* psl)
{
	assert(psl);
	assert(psl->size > 0);
	int i = 0;
	for (i = 0; i <= psl->size - 1;i++)
	{
		psl->array[i] = psl->array[i + 1];
	}
	psl->size--;
}

// ˳�������
int SeqListFind(SL* psl, SeqListData x)
{
	assert(psl);
	for (int i = 0; i < psl->size; i++)
	{
		if (psl->array[i] == x)
		{
			return i;
		}
	}
	return -1;
}

// ˳�����posλ�ò���x
void SeqListInsert(SL* psl, size_t pos, SeqListData x)
{
	assert(psl);
	assert(pos >= 0 && pos <= psl->size);
	CheckCapacity(psl);
	int start ;
	int end;
	for (start = pos,end=psl->size; start < end; end--)
	{
		psl->array[end] = psl->array[end-1];
	}
	psl->array[pos] = x;
	psl->size++;
}

// ˳���ɾ��posλ�õ�ֵ
void SeqListErase(SL* psl, size_t pos)
{
	assert(psl);
	assert(pos >= 0 && pos <= psl->size);
	int start;
	int end;
	for (start = pos, end = psl->size; start < end; start++)
	{
		psl->array[start] = psl->array[start + 1];
	}
	psl->size--;
}

