#include"SeqList.h"

void test_of_pushback()
{
	SL psl;
	SeqListInit(&psl);
	SeqListPushBack(&psl, 0);
	SeqListPushBack(&psl, 1);
	SeqListPushBack(&psl, 2);
	SeqListPrint(&psl);
	SeqListDestory(&psl);
}
void test_of_pushfront()
{
	SL psl;
	SeqListInit(&psl);
	SeqListPushBack(&psl, 0);
	SeqListPushBack(&psl, 1);
	SeqListPushBack(&psl, 2);
	SeqListPushFront(&psl, 10);
	SeqListPushFront(&psl, 20);
	SeqListPushFront(&psl, 30);
	SeqListPrint(&psl);
	SeqListDestory(&psl);
}

void test_of_popback()
{
	SL psl;
	SeqListInit(&psl);
	SeqListPushBack(&psl, 0);
	SeqListPushBack(&psl, 1);
	SeqListPushBack(&psl, 2);
	SeqListPrint(&psl);
	SeqListPopBack(&psl);
	SeqListPrint(&psl);
	SeqListDestory(&psl);
}

void test_of_popfront()
{
	SL psl;
	SeqListInit(&psl);
	SeqListPushBack(&psl, 0);
	SeqListPushBack(&psl, 1);
	SeqListPushBack(&psl, 2);
	SeqListPrint(&psl);
	SeqListPopFront(&psl);
	SeqListPrint(&psl);
	SeqListDestory(&psl);
}

void test_of_find()
{
	SL psl;
	SeqListInit(&psl);
	SeqListPushBack(&psl, 0);
	SeqListPushBack(&psl, 1);
	SeqListPushBack(&psl, 2);
	SeqListPrint(&psl);
	int i = SeqListFind(&psl, 2);
	printf("%d ", i);
	i = SeqListFind(&psl, 3);
	printf("%d ", i);
	SeqListDestory(&psl);
}

void test_of_insert()
{
	SL psl;
	SeqListInit(&psl);
	SeqListPushBack(&psl, 0);
	SeqListPushBack(&psl, 1);
	SeqListPushBack(&psl, 2);
	SeqListPrint(&psl);
	SeqListInsert(&psl, 2, 3);
	SeqListPrint(&psl);
	SeqListDestory(&psl);
}

void test_of_erase()
{
	SL psl;
	SeqListInit(&psl);
	SeqListPushBack(&psl, 0);
	SeqListPushBack(&psl, 1);
	SeqListPushBack(&psl, 2);
	SeqListPrint(&psl);
	SeqListErase(&psl, 1);
	SeqListPrint(&psl);
	SeqListDestory(&psl);
}

int main()
{
	//test_of_pushback();
	//test_of_pushfront();
	//test_of_popback();
	//test_of_popfront();
	//test_of_find();
	//test_of_insert();
	test_of_erase();
	return 0;
}