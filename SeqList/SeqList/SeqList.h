#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
typedef int SeqListData;

typedef struct SeqList
{
	SeqListData* array;//顺序表数组
	int size;//顺序表当前长度
	int capacity;//顺序表总容量
}SL;

//初始化顺序表
void SeqListInit(SL* psl);
// 检查空间，如果满了，进行增容
void CheckCapacity(SL* psl);
// 顺序表尾插
void SeqListPushBack(SL* psl, SeqListData x);
// 顺序表尾删
void SeqListPopBack(SL* psl);
// 顺序表头插
void SeqListPushFront(SL* psl, SeqListData x);
// 顺序表头删
void SeqListPopFront(SL* psl);
// 顺序表查找
int SeqListFind(SL* psl, SeqListData x);
// 顺序表在pos位置插入x
void SeqListInsert(SL* psl, size_t pos, SeqListData x);
// 顺序表删除pos位置的值
void SeqListErase(SL* psl, size_t pos);
// 顺序表销毁
void SeqListDestory(SL* psl);
// 顺序表打印
void SeqListPrint(SL* psl);